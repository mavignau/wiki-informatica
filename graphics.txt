Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2019-01-17T01:52:35-03:00

====== graphics ======
Created jueves 17 enero 2019

@icons
https://webkul.github.io/vivid/
Set de iconos SVG muy customizables. ¿Y qué tiene de distinto esta pack comparados con los demás? Pues que son en bicolor :wink:
Puedes definir un par de colores, uno principal y uno secundario, y los iconos que muestres se verán con esos dos colores. En la web que os he dejado arriba tenéis algunos ejemplos.
