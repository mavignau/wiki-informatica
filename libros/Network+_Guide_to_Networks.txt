Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2019-01-17T12:39:17-03:00

====== Network+ Guide to Networks ======
Created jueves 17 enero 2019

Network+ Guide to Networks (Mindtap Course List) 8th Edition by Jill West, Tamara Dean, Jean Andrews #Network+ #Network #Networks
--------------------
Master the technical skills and industry knowledge you need to begin an exciting career installing, configuring and troubleshooting computer networks with West/Dean/Andrews' NETWORK+ GUIDE TO NETWORKS, 8th edition. It thoroughly prepares you for success on CompTIA's Network+ N10-007 certification exam with fully mapped coverage of all objectives, including protocols, topologies, hardware, network design, security and troubleshooting. Virtualization-based projects give you experience working with a wide variety of hardware, software, operating systems and device interactions, while "On the Job" stories, Applying Concepts activities, and Hands-On and Capstone Projects let you explore concepts in more depth. MindTap Networking offers additional practice and certification prep. The text's emphasis on real-world problem solving provides the tools for success in any computing environment.
--------------------
2018 | Inglés | PDF | 881 páginas | 42,2 MB
--------------------
